package com.thty.mysearch

import androidx.lifecycle.viewModelScope
import com.thty.mysearch.api.ApiService
import com.thty.mysearch.di.networkModule
import com.thty.mysearch.di.viewModelModule
import com.thty.mysearch.viewmodel.MainViewModel
import com.thty.mysearch.viewmodel.SearchViewModel
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

import org.koin.core.context.startKoin
import org.koin.test.KoinTest
import org.koin.test.get
import retrofit2.await

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@ExperimentalCoroutinesApi
class ExampleUnitTest : KoinTest {
    private val testDispatcher = TestCoroutineDispatcher()

    @Test
    fun myTest(){
        startKoin { modules(listOf(networkModule, viewModelModule)) }

        val api: ApiService = get()

        runBlocking {
            api.getBooks("설현", page= 1, size = 20).apply {
                val c = this
            }
        }
        assertTrue(true)
    }
}
