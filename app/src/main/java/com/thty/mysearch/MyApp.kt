package com.thty.mysearch

import android.app.Application
import com.thty.mysearch.di.networkModule
import com.thty.mysearch.di.repositoryModule
import com.thty.mysearch.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@MyApp)
            modules(listOf(viewModelModule, networkModule, repositoryModule))
        }
    }
}