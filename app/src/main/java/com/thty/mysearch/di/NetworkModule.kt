package com.thty.mysearch.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.thty.mysearch.BuildConfig
import com.thty.mysearch.api.ApiInterceptor
import com.thty.mysearch.api.ApiService
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit

val networkModule = module {
    single {
        Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(Json.asConverterFactory(MediaType.parse("application/json")!!))
                .client(get())
                .build()
    }

    single {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }
        builder.addInterceptor(ApiInterceptor())
        builder.build()
    }

    single {
        get<Retrofit>().create(ApiService::class.java)
    }
}