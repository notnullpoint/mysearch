package com.thty.mysearch.di

import com.thty.mysearch.MyApp
import com.thty.mysearch.repository.RepositoryImageImpl
import com.thty.mysearch.viewmodel.MainViewModel
import com.thty.mysearch.viewmodel.SearchViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { SearchViewModel(get()) }

    single { androidApplication() as MyApp }
}

val repositoryModule = module {
    factory { RepositoryImageImpl(get()) }
}
