package com.thty.mysearch.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.thty.mysearch.BuildConfig
import com.thty.mysearch.R
import com.thty.mysearch.databinding.LayoutImageBinding
import com.thty.mysearch.model.Documents
import com.thty.mysearch.viewmodel.SearchViewModel
import com.thty.mysearch.viewmodel.SearchViewModel.Companion.DEFAULT_FILTER

class SearchAdapter(var viewModel: SearchViewModel) : RecyclerView.Adapter<SearchAdapter.ImageHolder>(), Filterable {
    private var items = ArrayList<Documents>()
    var filteredList = ArrayList<Documents>()

    fun addItem(items: ArrayList<Documents>?){
        this.items.addAll(items ?: ArrayList())
        filter.filter(viewModel.filterLiveData.value)
    }

    fun clear(){
        this.items.clear()
        this.filteredList.clear()
        filter.filter(DEFAULT_FILTER)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val binding = LayoutImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageHolder(binding)
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        val document = filteredList[position]
        holder.binding.ivBook.load(document.thumbnailUrl){
            crossfade(true)
            placeholder(R.drawable.ic_photo)
        }

        holder.binding.tvDebugCollections.apply {
            visibility = if(BuildConfig.DEBUG) View.VISIBLE else View.GONE
            text = document.collection
        }
    }


    inner class ImageHolder(var binding: LayoutImageBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                viewModel.isLoading.postValue(true)
                val filterName = constraint.toString()
                val filterResults = FilterResults()
                filterResults.values = viewModel.getFilterList(filterName, items)
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                filteredList = results.values as ArrayList<Documents>
                notifyDataSetChanged()
                viewModel.isLoading.value = false
            }
        }
    }
}