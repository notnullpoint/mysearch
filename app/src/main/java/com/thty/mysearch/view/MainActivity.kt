package com.thty.mysearch.view

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import com.thty.mysearch.BR
import com.thty.mysearch.R
import com.thty.mysearch.base.BaseActivity
import com.thty.mysearch.databinding.ActivityMainBinding
import com.thty.mysearch.utils.onClick
import com.thty.mysearch.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity: BaseActivity<ActivityMainBinding, MainViewModel>() {
    override val bindingVariable: Int
        get() = BR.vm
    override val layoutId: Int
        get() = R.layout.activity_main
    override val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startAni()

        binding.vSearchBar.onClick {
            val option  =
                ActivityOptionsCompat
                    .makeSceneTransitionAnimation(this@MainActivity,
                        Pair.create(binding.tvClickView as View, ViewCompat.getTransitionName(binding.tvClickView)!!),
                        Pair.create(binding.ivSearch as View, ViewCompat.getTransitionName(binding.ivSearch)!!)
                    )

            startActivity(Intent(this, SearchActivity::class.java), option.toBundle())
        }
    }

    private fun startAni() {
        binding.vSearchBar.apply{
            alpha = 0f
            scaleX = 0.5f
            scaleY = 0.5f
            binding.vSearchBar.animate().apply {
                duration = 400
                alpha = 1f
                scaleX = 1f
                scaleY = 1f
                setListener(object : Animator.AnimatorListener{
                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                        viewModel.startTyping()
                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                })
                start()
            }
            visibility = View.VISIBLE
        }
    }
}