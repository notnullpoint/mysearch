package com.thty.mysearch.view

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thty.mysearch.BR
import com.thty.mysearch.R
import com.thty.mysearch.base.BaseActivity
import com.thty.mysearch.databinding.ActivitySearchBinding
import com.thty.mysearch.model.Documents
import com.thty.mysearch.utils.onClick
import com.thty.mysearch.view.adapter.SearchAdapter
import com.thty.mysearch.view.listener.EndlessRecyclerViewScrollListener
import com.thty.mysearch.viewmodel.SearchViewModel
import com.thty.mysearch.viewmodel.SearchViewModel.Companion.DEFAULT_FILTER
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchActivity : BaseActivity<ActivitySearchBinding, SearchViewModel>(){
    override val bindingVariable: Int
        get() = BR.vm
    override val layoutId: Int
        get() = R.layout.activity_search
    override val viewModel: SearchViewModel by viewModel()

    private val adapter: SearchAdapter by lazy {
        SearchAdapter(viewModel)
    }

    private val scrollListener by lazy {
        object : EndlessRecyclerViewScrollListener(binding.rvView.layoutManager as GridLayoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if(!viewModel.isLastPage()){
                    viewModel.getList(false) // page 는 0 부터 시작
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.etView.apply {
            GlobalScope.launch(Dispatchers.Main) {
                delay(500)
                requestFocus()
                with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager){
                    showSoftInput(this@apply, 0)
                }
            }
            setOnFocusChangeListener { _, hasFocus ->
                binding.fab.visibility = if(hasFocus) View.GONE else View.VISIBLE
            }

            setOnEditorActionListener(object : TextView.OnEditorActionListener{
                override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                        with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager){
                            hideSoftInputFromWindow(windowToken, 0)
                        }
                        clearFocus()
                        adapter.clear()
                        viewModel.getList(true) // 1첫 로드
                        return true
                    }
                    return false
                }
            })
        }

        binding.rvView.apply {
            adapter = this@SearchActivity.adapter
            addOnScrollListener(scrollListener)
        }

        binding.fab.onClick {
            showFilterPopup(it)
        }

        setObserver()
    }

    private fun showFilterPopup(v: View) {
        val popup = PopupMenu(this, v)
        with(viewModel.getCollectionToList()) {
            for (collection in this)
                popup.menu.add(collection)
        }
        popup.setOnMenuItemClickListener {
            adapter.filter.filter(it.title)
            binding.rvView.scrollToPosition(0) // 필터 적용시 최상단으로 이동
            scrollListener.resetState() //필터를 적용하면 리스트 사이즈가 변경되서 리셋 해줘야 한다.
            true
        }
        popup.show()
    }

    private fun setObserver() {
        with(viewModel){
            apiLiveData.observe(this@SearchActivity, Observer {
                adapter.addItem(it)
            })

            filterLiveData.observe(this@SearchActivity, Observer {
                binding.fab.isSelected = it != DEFAULT_FILTER
            })

            toastLiveData.observe(this@SearchActivity, Observer {
                Toast.makeText(this@SearchActivity, it, Toast.LENGTH_LONG).show()
            })
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // Checks the orientation of the screen
        with(binding.rvView.layoutManager as GridLayoutManager){
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                spanCount = 3
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                spanCount = 2
            }
        }
    }
}