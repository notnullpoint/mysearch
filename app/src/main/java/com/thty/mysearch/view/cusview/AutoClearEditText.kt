package com.thty.mysearch.view.cusview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.View.OnTouchListener
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.thty.mysearch.R

class AutoClearEditText : AppCompatEditText, TextWatcher, OnTouchListener, OnFocusChangeListener {
    private var mIsClearDrawable: Drawable? = null
    private var onTouchListener: OnTouchListener? = null
    private var mIsClearVisible = false

    private var externalFocusChangeListener: OnFocusChangeListener? = null

    constructor(context: Context?) : super(context) {
        init(null)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    override fun setOnFocusChangeListener(onFocusChangeListener: OnFocusChangeListener) {
        this.externalFocusChangeListener = onFocusChangeListener
    }

    override fun setOnTouchListener(onTouchListener: OnTouchListener) {
        this.onTouchListener = onTouchListener
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            context.obtainStyledAttributes(attrs, R.styleable.AutoClearEditText).apply {
                if (hasValue(R.styleable.AutoClearEditText_clearVisible)) {
                    mIsClearVisible = getBoolean(R.styleable.AutoClearEditText_clearVisible, false)
                }
                if (hasValue(R.styleable.AutoClearEditText_clearImg)) {
                    mIsClearDrawable = getDrawable(R.styleable.AutoClearEditText_clearImg)
                }
                recycle()
            }
        }

        if (mIsClearVisible) {
            if (mIsClearDrawable == null) { //기본 삭제 이미지
                val tempDrawable = ContextCompat.getDrawable(context, R.drawable.ic_edit_del)
                mIsClearDrawable = DrawableCompat.wrap(tempDrawable!!)
            }
            mIsClearDrawable!!.setBounds(
                0,
                0,
                mIsClearDrawable!!.intrinsicWidth,
                mIsClearDrawable!!.intrinsicHeight
            )
            setClearIconVisible(false)
        }

        addTextChangedListener(this)
        super.setOnTouchListener(this)
        super.setOnFocusChangeListener(this)
    }


    override fun onFocusChange(view: View, hasFocus: Boolean) {
        if (mIsClearVisible) {
            if (hasFocus) {
                setClearIconVisible(text!!.isNotEmpty())
            } else {
                setClearIconVisible(false)
            }
        }
        if (externalFocusChangeListener != null) {
            externalFocusChangeListener!!.onFocusChange(view, hasFocus)
        }

        isCursorVisible = hasFocus
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        val x = motionEvent.x.toInt()
        if (mIsClearVisible && mIsClearDrawable!!.isVisible && x > width - paddingRight - mIsClearDrawable!!.intrinsicWidth) {
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                setText("")
                return true
            }
        }
        return if (onTouchListener != null) {
            onTouchListener!!.onTouch(view, motionEvent)
        } else {
            false
        }
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (mIsClearVisible && isFocused) {
            setClearIconVisible(s.isNotEmpty())
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun afterTextChanged(s: Editable) {}

    private fun setClearIconVisible(visible: Boolean) {
        mIsClearDrawable!!.setVisible(visible, false)
        setCompoundDrawables(null, null, if (visible) mIsClearDrawable else null, null)
    }
}