package com.thty.mysearch.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.thty.mysearch.api.ResultWrapper
import com.thty.mysearch.base.BaseViewModel
import com.thty.mysearch.model.Documents
import com.thty.mysearch.model.Meta
import com.thty.mysearch.repository.RepositoryImageImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel(var repo: RepositoryImageImpl) : BaseViewModel() {


    private var page = START_PAGE
    private var lastMeta = Meta()
    //xml 에서 EditText 값을 담는다
    var searchText: String = ""

    val isIconVisible = MutableLiveData(true)
    val isLoading = MutableLiveData(false)

    val filterLiveData by lazy { MutableLiveData(DEFAULT_FILTER) }
    val toastLiveData by lazy { MutableLiveData<String>() }

    private val collections by lazy { LinkedHashSet<String>() }

    val apiLiveData: MutableLiveData<ArrayList<Documents>> by lazy {
        MutableLiveData<ArrayList<Documents>>()
    }

    fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        isIconVisible.value = s.isNullOrEmpty()
    }

    fun getList(init: Boolean){
        if(init){
            page = START_PAGE
            collections.clear()
            collections.add(DEFAULT_FILTER)
        }
        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO){
            var collectionsCnt = 0
            val arrayList = ArrayList<Documents>()
            while (collectionsCnt < SIZE){
                repo.getImages(searchText, page= page++, size = SIZE).apply {
                    when(this){
                        is ResultWrapper.NetworkError -> errorMsg()
                        is ResultWrapper.GenericError -> errorMsg()
                        is ResultWrapper.Success -> {
                            val documents = this.value.documents
                            for(doc in documents){
                                with(filterLiveData.value){
                                    collectionsCnt += if(this == DEFAULT_FILTER || this == doc.collection) 1 else 0
                                    collections.add(doc.collection)
                                }
                            }
                            arrayList.addAll(documents)
                            lastMeta = this.value.meta
                            Log.d("--> loadApi", "collectionsCnt : $collectionsCnt")
                        }
                    }
                }
                if(lastMeta.isEnd) {
                    Log.d("--> loadApi", "finish")
                    toastLiveData.postValue("마지막 페이지 입니다.")
                    break
                }
            }
            apiLiveData.postValue(arrayList)
            isLoading.postValue(false)
        }
    }

    private fun errorMsg(){
         val msg = "알 수 없는 오류로 더 이상 불러올 수 없습니다."
        lastMeta.isEnd = true
        Log.d("--> loadApi", "finish error")
        toastLiveData.postValue(msg)
        isLoading.postValue(false)
    }

    fun isLastPage(): Boolean {
        return lastMeta.isEnd
    }

    fun getCollectionToList(): List<String>{
        return ArrayList(collections)
    }

    fun getFilterList(filterName: String, items: ArrayList<Documents>) : ArrayList<Documents> {
        return if (filterName.isEmpty() || filterName == DEFAULT_FILTER) {
            this.filterLiveData.postValue(DEFAULT_FILTER)
            items
        } else {
            val filteringList: ArrayList<Documents> = ArrayList()
            this.filterLiveData.postValue(filterName)
            items
                .filter { it.collection == filterName }
                .forEach { it2 ->
                    filteringList.add(it2)
                }
            filteringList
        }
    }

    companion object{
        const val SIZE = 40
        const val START_PAGE = 1
        const val DEFAULT_FILTER = "all"
    }
}