package com.thty.mysearch.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.thty.mysearch.MyApp
import com.thty.mysearch.R
import com.thty.mysearch.base.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel(private val app: MyApp) : BaseViewModel() {

    private val constHint: String by lazy {
        app.getString(R.string.search_hint)
    }

    var hintLiveData = MutableLiveData("")

    fun startTyping() {
        hintLiveData.value = ""
        viewModelScope.launch {
            for (letter in constHint) {
                delay(100)
                hintLiveData.value += letter
            }
        }
    }
}