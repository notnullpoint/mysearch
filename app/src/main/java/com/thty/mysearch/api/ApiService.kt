package com.thty.mysearch.api

import com.thty.mysearch.model.Respose
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/v2/search/image")
    suspend fun getImages(
        @Query(value = "query", encoded = true) query: String,
        @Query("page") page: Int,
        @Query("size") size: Int): Respose
}
