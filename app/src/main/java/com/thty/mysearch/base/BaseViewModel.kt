package com.thty.mysearch.base

import androidx.lifecycle.ViewModel


//당장 쓸 일이 없지만, 나중에 필요해 질 경우가 생기기 때문에 base 를 잡느다
abstract class  BaseViewModel : ViewModel()