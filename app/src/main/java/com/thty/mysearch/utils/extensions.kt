package com.thty.mysearch.utils

import android.view.View
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.actor

@OptIn(ObsoleteCoroutinesApi::class)
fun View.onClick(action: suspend (View) -> Unit) {
    val event = GlobalScope.actor<View>(Dispatchers.Main) {
        for (event in channel) action(event)
    }

    setOnClickListener {
        event.offer(it)
    }
}