package com.thty.mysearch.utils

import android.widget.ImageView
import androidx.annotation.Keep
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load


@Keep
object BindingAdapters {

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImageUrl(imageView: ImageView, url: String?) {
        if(url != null && url.isNotEmpty()) {
            imageView.load(url)
        }
    }

}
