package com.thty.mysearch.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Respose(
    @SerialName("meta")
    var meta: Meta,
    @SerialName("documents")
    var documents: ArrayList<Documents>
)