package com.thty.mysearch.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Meta(
    @SerialName("total_count")
    var totalCount : Int = 0,
    @SerialName("pageable_count")
    var pageableCount: Int = 0,
    @SerialName("is_end")
    var isEnd: Boolean = true
)