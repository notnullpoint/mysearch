package com.thty.mysearch.repository

import com.thty.mysearch.api.ApiService
import com.thty.mysearch.api.ResultWrapper
import com.thty.mysearch.api.safeApiCall
import com.thty.mysearch.model.Respose
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface ImageRepository {
    suspend fun getImages(query: String, page: Int, size: Int): ResultWrapper<Respose>
}

class RepositoryImageImpl(private val service: ApiService,
                     private val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ImageRepository {

    override suspend fun getImages(query: String, page: Int, size: Int): ResultWrapper<Respose> {
        return safeApiCall(dispatcher) { service.getImages(query = query, page = page , size = size) }
    }
}